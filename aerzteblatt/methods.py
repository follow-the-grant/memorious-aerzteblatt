import fingerprints
import logging
import re
from alephclient.api import AlephAPI
from datetime import datetime
from followthemoney import model
from urllib.parse import quote


log = logging.getLogger(__name__)

api = AlephAPI()

_ = lambda x: fingerprints.generate(x)  # noqa


PUBLISHER = 'Deutsches Ärzteblatt'


def parse_conflicts(html):
    conflict = ' '.join(' '.join(p.itertext()) for p in html.xpath('.//p[@class="Interessenkonflikt"]'))
    for author in html.xpath('.//span[@class="author"]'):
        yield ' '.join(author.itertext()), conflict


def process(context, data):
    m = re.match(r'^https://www.aerzteblatt.de/pdf\.asp\?id=(\d+)', data['url'])
    if m:
        with context.http.rehash(data) as result:
            if result.html is not None:
                pdf_id = m.groups()[0]
                pdf_url = result.html.xpath('.//div[@id="buttons"]/div[@class="save"]/a/@href')
                pdf_url = pdf_url[0] if pdf_url else None
                if pdf_url:
                    ds = context.datastore['aerzteblatt_documents']
                    ds.insert({
                        'pdf_id': pdf_id,
                        'pdf_url': quote(pdf_url.split('?')[0]),
                    })
    if re.match(r'^https://www.aerzteblatt.de/(archiv|int/archive/article)/\d+/.*', data['url']):
        with context.http.rehash(data) as result:
            if result.html is not None:
                ds = context.datastore['aerzteblatt_authors_conflicts']
                source_url = result.html.xpath('.//div[@class="archiveArticleInfo"]/ul/li[1]/a/@href')
                source_url = source_url[0] if source_url else ''
                m = re.match(r'^\/pdf\.asp\?id=(\d+)', source_url)
                if m:
                    pdf_id = m.groups()[0]
                    for author, coi in parse_conflicts(result.html):
                        ds.insert({
                            'article_url': data['url'],
                            'pdf_id': pdf_id,
                            'author': author,
                            'coi': coi,
                        })
    context.emit(data=data)


def enrich(context, data):
    ds_doc = context.datastore['aerzteblatt_documents']
    ds_authors = context.datastore['aerzteblatt_authors_conflicts']
    ds_cois = context.datastore['aerzteblatt_cois']
    source = ds_doc.find_one(pdf_url=quote(data['url'].split('?')[0]))
    if source:
        pdf_id = source['pdf_id']
        results = ds_authors.find(pdf_id=pdf_id)
        entities = []
        document_id = data['aleph_id']

        for res in results:
            try:
                last_name, first_name = res['author'].split(',')
                author_name = f'{first_name} {last_name}'
            except ValueError:
                last_name = first_name = None
                author_name = res['author']

            author = model.make_entity('Person')
            author.make_id(pdf_id, _(author_name))
            author.add('name', author_name)
            author.add('firstName', first_name)
            author.add('lastName', last_name)
            author.add('sourceUrl', res['article_url'])

            link = model.make_entity('Documentation')
            link.make_id('link', author.id, pdf_id)
            link.add('document', document_id)
            link.add('entity', author)
            link.add('role', 'author')
            link.add('sourceUrl', res['article_url'])

            entities += [author, link]

            if res['coi']:

                # the full coi statement
                coi = model.make_entity('PlainText')
                coi.make_id('coi', document_id)
                coi.add('title', 'conflict of interest statement (article)')
                coi.add('bodyText', res['coi'])
                coi.add('publisher', PUBLISHER)
                coi.add('sourceUrl', res['article_url'])
                coi.add('parent', document_id)
                coi.add('author', author_name)

                # link article -> coi statement
                coi_ref = model.make_entity('Documentation')
                coi_ref.make_id('coi_ref', document_id)
                coi_ref.add('publisher', PUBLISHER)
                coi_ref.add('document', document_id)
                coi_ref.add('entity', coi)
                coi_ref.add('role', 'conflict of interest statement')
                coi_ref.add('summary', res['coi'])

                # link author -> coi statement
                author_coi_ref = model.make_entity('Documentation')
                author_coi_ref.make_id('author_coi_ref', document_id, author.id)
                author_coi_ref.add('document', coi)
                author_coi_ref.add('entity', author)
                author_coi_ref.add('publisher', PUBLISHER)
                author_coi_ref.add('role', 'conflict of interest statement')
                author_coi_ref.add('summary', res['coi'])

                entities += [coi, coi_ref, author_coi_ref]

                # store in db for coi split later
                ds_cois.insert({
                    'aleph_id': document_id.split('.')[0],
                    'author_id': author.id,
                    'author': author_name,
                    'coi_statement': res['coi'],
                    'date_added': datetime.now().isoformat()
                })

        api.write_entities(data['aleph_collection_id'], entities)
